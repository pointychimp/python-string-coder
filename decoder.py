#!/usr/bin/python

import fileinput

# xxxx1 means unencoded
# xxxx2 means encoded
vowels1 = 'aeiouAEIOU' 
vowels2 = 'eiouaEIOUA'
#numbers1 = '1234567890'
#numbers2 = '6789012345'

doubleLetterChar = '/'

def decodeWord(word):
    if word.isalpha() or word.replace(doubleLetterChar, '').isalpha():
        # break off first letter from end
        # and turn string into list for easy manipulation
        firstLetter = word[len(word)-1]
        word = list(word[:-1])
        # fix capitalization
        # doing primitive check that word isn't weirdly capitalized
        # in which case leave it alone
        if len(word) and firstLetter.islower() and not word[0].islower():
            word[0] = word[0].lower()
            firstLetter = firstLetter.upper()
        # do sub-word operations
        for i, letter in enumerate(word):
            if letter == doubleLetterChar and i > 0:
                letter = word[i-1]
            elif i < len(word)-1:
                if word[i+1] != doubleLetterChar:
                    if letter in vowels2:
                        letter = vowels1[ vowels2.find(letter) ]
            elif i == len(word)-1 and letter in vowels2:
                letter = vowels1[ vowels2.find(letter) ]
            word[i] = letter
        word = firstLetter + ''.join(word)
    return word

def main():
	allLines = list(fileinput.input())
	for i, line in enumerate(allLines):
		words = list(line.split())
        for j, word in enumerate(words):
            word = word.strip()
            word = decodeWord(word)
            words[j] = word
        line = ' '.join(words)
        allLines[i] = line
	allLines = '\n'.join(allLines)
	print allLines

if __name__ == '__main__':
	main()