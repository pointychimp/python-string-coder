I'm learning python. This implements some string functions to apply the rules to a code I came up with a long time ago. 


Examples
--------

Encoding

	'Hello world' -> 'il/uh urldw'

Decoding example

	'Hit ekic si a oil' -> 'The cake is a lie'

Limitations
-----------

As of the first version, there are some limitations that don't make this "done" yet.

* Punctuation throws it off
* I would like 'sleeeeeep' to encode to 'le////ps' not 'le/e/ips'

Known Issues
------------

The double letter indicator is a punctuation mark that is commenly used. While that is what I'm used to, I should change it to something less commonly used. As it stands, 'good/bad' wouldn't be encoded (bad!), but I want it to. Once it gets encoded, I want to avoid the decoder taking 'o/dg/edb' and treating it a single word and returning something like 'boodggad'.