#!/usr/bin/python

import fileinput

# xxxx1 means unencoded
# xxxx2 means encoded
vowels1 = 'aeiouAEIOU' 
vowels2 = 'eiouaEIOUA'
#numbers1 = '1234567890'
#numbers2 = '6789012345'

doubleLetterChar = '/'

def encodeWord(word):
    # only work on words containing only letters for now
    if word.isalpha():
        # break off the first letter
        # and turn string into list for easy manipulation
        firstLetter = word[0]
        word = list(word[1:])
        # do sub-word operations
        for i, letter in enumerate(word):
            if i < len(word)-1:
                if letter == word[i+1]:
                    word[i+1] = doubleLetterChar
                elif letter in vowels1:
                    letter = vowels2[ vowels1.find(letter) ]
            else:
                if letter in vowels1:
                    letter = vowels2[ vowels1.find(letter) ]
            word[i] = letter
        # fix capitalization 
        # doing primitive check that word isn't weirdly capitalized
        # in which case leave it alone
        if len(word) and firstLetter.isupper() and not word[0].isupper():
            word[0] = word[0].upper()
            firstLetter = firstLetter.lower()
        # turn back into string
        word = ''.join(word) + firstLetter
        # get out of here!
    return word

def main():
    allLines = list(fileinput.input())
    for i, line in enumerate(allLines):
        words = list(line.split())
        for j, word in enumerate(words):
            word = word.strip()
            word = encodeWord(word)
            words[j] = word
        line = ' '.join(words)
        allLines[i] = line
    allLines = '\n'.join(allLines)
    print allLines

if __name__ == '__main__':
    main()